package main.utility;

public class Utilities
{
	public static boolean animaleSupportato(String razza)
	{
		// verifica che la razza inserita per l'animale
		// sia tra quelle supportate dal sistema
		if (razza.equals("GATTO") || 
			razza.equals("CANE") ||
			razza.equals("POLLO") ||
			razza.equals("CONIGLIO"))
		{
			return true;
		}
		return false;
	}
	
	public static boolean prenotazioniDisponibili(int numeroPrenotazioni)
	{
		if (numeroPrenotazioni < 4)
		{
			return true;			
		}
		return false;
	}
}
