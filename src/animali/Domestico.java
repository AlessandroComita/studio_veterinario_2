package animali;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Domestico extends Animale
{
	// ATTRIBUTI
	private String nome;
	
	// COSTRUTTORI
	public Domestico(LocalDate dataNascita, Razza razza)
	{
		super(dataNascita, razza);
	}
	
	public Domestico(LocalDate dataNascita, Razza razza, String nome)
	{
		super(dataNascita, razza);
		this.nome = nome;
	}

	
	// GETTERS+SETTERS
	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}
	
	// METODI
	@Override
	public void stampaAnimale()
	{
		super.stampaAnimale();
		System.out.println("Nome animale > " + this.nome); 
	}
}
