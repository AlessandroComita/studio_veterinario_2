package animali;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Animale
{
	// ATTRIBUTI
	protected LocalDate dataNascita;
	protected Razza razza;

	
	// COSTRUTTORI
	public Animale(LocalDate dataNascita, Razza razza)
	{
		super();
		this.dataNascita = dataNascita;
		this.razza = razza;
	}
	

	// GETTERS+SETTERS
	public LocalDate getDataNascita()
	{
		return dataNascita;
	}

	public void setDataNascita(LocalDate dataNascita)
	{
		this.dataNascita = dataNascita;
	}

	public Razza getRazza()
	{
		return razza;
	}

	public void setRazza(Razza razza)
	{
		this.razza = razza;
	}

	
	// METODI
	public void stampaAnimale()
	{
		System.out.println("Razza > " + this.razza.getRazza());
		DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		System.out.println("Data di nascita > " + this.dataNascita.format(formato));
	}
}
