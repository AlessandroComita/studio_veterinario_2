package animali;

public enum Razza
{
	CANE ("Cane"), 
	GATTO ("Gatto"), 
	POLLO ("Pollo"), 
	CONIGLIO ("Coniglio"), 
	PAPPAGALLO ("Pappagallo");

	// ATTRIBUTI
	private String razza;
	
	// COSTRUTTORI
	private Razza(String razza)
	{
		this.razza = razza;
	}

	// GETTERS
	public String getRazza()
	{
		return razza;
	}
}
