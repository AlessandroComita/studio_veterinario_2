package animali;

import java.time.LocalDate;

public class DaAllevamento extends Animale
{

	// COSTRUTTORE
	public DaAllevamento(LocalDate dataNascita, Razza razza)
	{
		super(dataNascita, razza);
	}
}