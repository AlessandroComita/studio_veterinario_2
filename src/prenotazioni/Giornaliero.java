package prenotazioni;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import animali.Animale;
import animali.DaAllevamento;
import animali.Domestico;
import animali.Razza;
import main.utility.Utilities;

public class Giornaliero 
{
	// ATTRIBUTI
	// elenco giornaliero delle prenotazioni
	private Elenco<Prenotazione> elenco = new Elenco<>();
	// numero totale delle prenotazioni
	int numeroPrenotazioni = 0;
	
	// METODI
	// «giornaliero» sta per management giornaliero delle visite dello studio veterinario
	public void creaGiornaliero()
	{
		// stampa data di oggi
		System.out.println("Prenotazioni in Data odierna");
		LocalDate dataOdierna = LocalDate.now();
	    DateTimeFormatter formatoData = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	    String dataOdiernaFormattata = dataOdierna.format(formatoData);
	    System.out.println(dataOdiernaFormattata);
	    System.out.println("------------------\n");
	    
	    // inserimento dati dell'animale per la prenotazione secondo il formato:
	    // data di nascita, razza, nome (se animale domestico), slot orario del ricovero, causa del ricovero
		creaPrenotazione(LocalDate.parse("2015-02-02"), Razza.CANE, "Fido", Orario.PRIMO_SLOT, CausaRicovero.FEBBRE);
		creaPrenotazione(LocalDate.parse("2019-05-20"), Razza.CONIGLIO, "", Orario.SECONDO_SLOT, CausaRicovero.FRATTURA);
		creaPrenotazione(LocalDate.parse("2022-08-20"), Razza.POLLO, "", Orario.TERZO_SLOT, CausaRicovero.INDIGESTIONE);
		creaPrenotazione(LocalDate.parse("2019-12-15"), Razza.GATTO, "Kitty", Orario.QUARTO_SLOT, CausaRicovero.FEBBRE);
		creaPrenotazione(LocalDate.parse("2017-01-06"), Razza.CANE, "Jack", Orario.QUINTO_SLOT, CausaRicovero.FRATTURA);
		
		// visualizza su schermo l'elenco odierno delle prenotazioni 
		stampaPrenotazioni();
	}
	
	public void creaPrenotazione(LocalDate dataNascita, Razza razza, 
			String nome, Orario orario, CausaRicovero causa)
			{
				// crea istanza generica dell'animale
				Animale paziente;
				
				// controlla la razza inserita per l'animale corrente;
				// se la razza è «cane» o «gatto», crea animale domestico
				// altrimenti crea animale da allevamento
				if (razza.getRazza().equals("Cane") || razza.getRazza().equals("Gatto"))
				{
					paziente = new Domestico(dataNascita, razza, nome);
				}
				else
				{
					paziente = new DaAllevamento(dataNascita, razza);	
				}

				// prima di "salvare" la prenotazione
				// assicurati che sia accettabile
				try
				{

					// ottieni stringa relativa alla razza del paziente corrente
					String temp = paziente.getRazza().toString();
					
					// verifica che la razza inserita sia fra quelle previste dal sistema
					// se la condizione non è verificata, lancia una eccezione
					if (!Utilities.animaleSupportato(temp))
					{
						throw new PrenotazioneException("Animale non supportato");
					}
					// verifica che siano ancora disponibili slot orari a cui assegnare le prenotazioni
					// se la condizione non è verificata, lancia una eccezione
					else if (!Utilities.prenotazioniDisponibili(numeroPrenotazioni))
					{
						throw new PrenotazioneException("Slot prenotazione esauriti"); 
					}
					else
					{
						// se le verifiche non hanno rilevato anomalie
						// convalida la prenotazione
						Prenotazione prenotazione = new Prenotazione(orario, causa, paziente);
						elenco.add(numeroPrenotazioni, prenotazione);
						numeroPrenotazioni++;
					}
				}
				catch (PrenotazioneException e)
				{
					e.printStackTrace();
				}
			}
	
	public void stampaPrenotazioni()
	{
		for (int i = 0; i < numeroPrenotazioni; i++)
		{
			elenco.get(i).stampaPrenotazione();
		}
	}
}
