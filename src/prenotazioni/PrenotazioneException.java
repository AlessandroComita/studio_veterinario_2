package prenotazioni;

public class PrenotazioneException extends Exception
{
	public PrenotazioneException(String messaggio)
	{
		super("Problema con la prenotazione");
		System.out.println(messaggio);
	}
}
