package prenotazioni;

public enum CausaRicovero
{
	FRATTURA ("Frattura"), 
	FEBBRE ("Febbre"), 
	INDIGESTIONE ("Indigestione");

	// ATTRIBUTI
	private String causa;

	// COSTRUTTORE
	CausaRicovero(String causa)
	{
		this.causa = causa;
	}

	// GETTERS
	public String getCausa()
	{
		return causa;
	}
}
