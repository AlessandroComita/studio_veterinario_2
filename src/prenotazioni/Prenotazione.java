package prenotazioni;

import animali.Animale;
import animali.DaAllevamento;
import animali.Domestico;

public class Prenotazione
{
	Animale animale;
	Domestico domestico;
	DaAllevamento daAllevamento;
	Orario orario;
	CausaRicovero causaRicovero;
	
	// COSTRUTTORE
	public Prenotazione(Orario orario, CausaRicovero causaRicovero, Animale animale)
	{
		super();
		this.animale = animale;
		this.orario = orario;
		this.causaRicovero = causaRicovero;
	}
	
	public Prenotazione(Orario orario, CausaRicovero causaRicovero, Domestico domestico)
	{
		super();
		this.domestico = domestico;
		this.orario = orario;
		this.causaRicovero = causaRicovero;
	}
	
	
	public Prenotazione(Orario orario, CausaRicovero causaRicovero, DaAllevamento daAllevamento)
	{
		super();
		this.setDaAllevamento(daAllevamento);
		this.orario = orario;
		this.causaRicovero = causaRicovero;
	}
	
	
	
	
	// GETTERS+SETTERS
	
	public DaAllevamento getDaAllevamento()
	{
		return daAllevamento;
	}

	public void setDaAllevamento(DaAllevamento daAllevamento)
	{
		this.daAllevamento = daAllevamento;
	}
	
	
	public Animale getAnimale()
	{
		return animale;
	}

	public void setAnimale(Animale animale)
	{
		this.animale = animale;
	}
	
	public Domestico getDomestico()
	{
		return domestico;
	}

	public void setDomestico(Domestico domestico)
	{
		this.domestico = domestico;
	}
	

	public Orario getOrario()
	{
		return orario;
	}

	public void setOrario(Orario orario)
	{
		this.orario = orario;
	}

	public CausaRicovero getCausaRicovero()
	{
		return causaRicovero;
	}

	public void setCausaRicovero(CausaRicovero causaRicovero)
	{
		this.causaRicovero = causaRicovero;
	}
	
	// METODI
	public void stampaPrenotazione()
	{
		System.out.println("");
		System.out.println("----------------------------------");
		System.out.println("Slot orario > " + orario.getOrario());
		System.out.println("Causa ricovero > " + causaRicovero.getCausa());
		animale.stampaAnimale();
		System.out.println("----------------------------------");
	}
}