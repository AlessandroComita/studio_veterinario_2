package prenotazioni;

public class Elenco<T>
{
	private T[] items = (T[]) new Object[12];
	private int count;
	
	// METODI
	// aggiungere un elemento all'elenco automaticamente
	public void add(T item)
	{
		items[count++] = item;
	}
	
	// aggiungere un elemento all'elenco secondo indice specificato
	public void add(int index, T item)
	{
		items[index] = item;
	}
	
	// get
	public T get(int index)
	{
		return items[index];
	}
}
