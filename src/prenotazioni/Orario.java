package prenotazioni;

public enum Orario
{
	PRIMO_SLOT ("8:00 / 9:00"),
	SECONDO_SLOT ("9:00 / 10:00"),
	TERZO_SLOT ("10:00 / 11:00"),
	QUARTO_SLOT ("11:00 / 12:00"), 
	QUINTO_SLOT ("12:00 / 13:00");

	// ATTRIBUTI
	private String orario;
	
	// COSTRUTTORE
	private Orario(String orario)
	{
		this.orario = orario;
	}

	// GETTER
	public String getOrario()
	{
		return orario;
	}
}
